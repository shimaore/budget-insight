Budget Insight API
------------------

This package implements some of the functions of the [Budget Insight API](https://console.budget-insight.com/documentation).

You can register for a free developer account [at Budget Insight](https://console.budget-insight.com/auth/register).
This will give you access to the documentation.
Be careful that some parameters and responses are not necessarily well-described.

This package is meant to be executed server-side (e.g. inside Node.js) as a trusted client.
It is not meant to be executed in the browser. (The authentication model is not designed with that goal in mind.)

As a reminder, typical parameters for all queries include:
- `offset`
- `limit`
- `min_date`
- `max_date`
- `expand`

Installation
------------

```shell
npm install budget-insight
```

```shell
yarn add budget-insight
```

Setup
-----

When instantiating this package, you will need parameters provided by Budget Insight.

Some parameters can be found in the Budget Insight Console for your domain
(provided for example in `https://console.budget-insight.com/domains/demo-sandbox.biapi.pro/settings`):

- Settings > API URL → `url` — URL prefix to the API (e.g. `https://demo-sandbox.biapi.pro/2.0`)
- Settings > Manage token config    → `config_token`
- Settings > Manage token users     → `users_token`
- Settings > Manage token invoicing → `invoicing_token`

Other parameters are specific to one of your applications and can be found in the Console for that application
(provided for example in `https://console.budget-insight.com/domains/demo-sandbox.biapi.pro/client-applications`):

- Client Applications > Client ID → `client_id`
- Client Applications > Client Secret → `client_secret`

Usage
-----

Once you have collected the various parameters you can instantiate an agent:

```javascript
const BudgetInsight = require('budget-insight')
const agent = BudgetInsight(url,{client_id,client_secret,config_token,users_token,invoicing_token})
```
