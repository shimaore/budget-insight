    Agent = require '..'
    ({expect} = require 'chai').should()
    exact = require 'superagent-jsonparse'
    Decimal = require 'decimal.js-light'

    decimalReviver = (text) -> new Decimal text

    describe 'The BI module', ->

The test environment must set those up for the tests to pass.

      agent = Agent process.env.BI_API_URL,
        client_id:        process.env.BI_APP_ID
        client_secret:    process.env.BI_APP_SECRET
        config_token:     process.env.BI_TOKEN_CONFIG
        users_token:      process.env.BI_TOKEN_USERS
        invoicing_token:  process.env.BI_TOKEN_INVOICING

      @timeout 4000

      it 'should provide an agent', ->
        @timeout 8*1000
        res = await agent.config().getBanks()
        res.should.be.an 'array'
        res.should.have.lengthOf.above 10
        res[0].should.have.property 'name'
        # await (require 'fs/promises').writeFile 'connectors.json',
        #   JSON.stringify res, null, '  '

      it 'should provide an agent (2) (inexact)', ->
        res = await agent.config().getBank 59
        res.should.have.property 'name', 'Connecteur de test'
        res.should.have.property 'id', 59

      it 'should provide an agent (2) (inexact, take two)', ->
        res = await agent.config()
          .getBank 59
          .use exact()
        res.should.have.property 'name', 'Connecteur de test'
        res.should.have.property 'id', 59

      it 'should provide an agent (2) (string)', ->
        res = await agent.config()
          .getBank 59
          .use exact (text) -> text
        res.should.have.property 'name', 'Connecteur de test'
        res.should.have.deep.property 'id', '59'

      it 'should provide an agent (2) (exact)', ->
        res = await agent.config()
          .getBank 59
          .use exact decimalReviver
        res.should.have.property 'name', 'Connecteur de test'
        res.should.have.deep.property 'id', new Decimal 59

      it 'should provide a webview', ->
        await agent.config().get '/auth/webview/'

      it 'should get config', ->
        res = await agent.config().get '/config'
        res.should.have.property 'autosync.enabled', '1'

      it 'should list users', ->
        res = await agent.users().getUsers()
        res.should.have.property 'users'

      it 'should list users (all)', ->
        res = await agent.users().getUser 'all'
        res.should.have.property 'users'

      it 'should attempt to log a user', ->
        @timeout 10*60*1000
        client_agent = await agent().addPermanentUser()
        try
          # 14 = CMB
          await client_agent.addConnection '3564637', '827281', 14, website:'pro'
        catch {code}
          yes
        code.should.equal 'wrongpass'
        res = await client_agent.getConnections()
        res.should.have.property 'connections'
        return

      it 'should inject data (exact)', ->
        @timeout 10*60*1000
        client_agent = await agent().addPermanentUser()
        banks = await client_agent.getBanks()
        compte = banks.filter ({id}) -> id is new Decimal 59
        connection = await client_agent.addConnection 'bob', '1234', 59
        connection.should.have.property 'id_bank', 59
        connection.should.have.property 'id_connector', 59
        account1 = await client_agent.addAccount connection, 'Compte chèque', '7181728', 2343.78, 'checking'
        account2 = await client_agent.addAccount connection, 'Compte chèque 2', '8182829', 501.46, 'checking'

        accounts = await client_agent.getAccounts().use exact decimalReviver
        accounts.should.have.property 'balance'
        accounts.should.have.property 'accounts'

        accounts = await client_agent
          .get "/users/me/accounts"
          .use exact decimalReviver
        accounts.should.have.property 'balance'
        accounts.should.have.property 'total'
        expect accounts.total.equals new Decimal 2

        return

      it 'should inject data (inexact)', ->
        @timeout 10*60*1000
        client_agent = await agent().addPermanentUser()
        console.log "\nCreated user #{client_agent.id_user} with token #{client_agent.auth_token}\n"
        banks = await client_agent.getBanks()
        # return console.log JSON.stringify await client_agent.get '/banks/14/fields'
        # return console.log JSON.stringify banks
        # providers = await client_agent.get '/providers'
        # Providers are e.g. Skype, …
        compte = banks.filter ({id}) -> id is 59
        connection = await client_agent.addConnection 'bob', '1234', 59
        connection.should.have.property 'id_bank', 59
        connection.should.have.property 'id_connector', 59
        account1 = await client_agent.addAccount connection, 'Compte chèque', '7181728', 2343.78, 'checking'
        account2 = await client_agent.addAccount connection, 'Compte chèque 2', '8182829', 501.46, 'checking'

        accounts = await client_agent.getAccounts()
        accounts.should.have.property 'balance'
        accounts.should.have.property 'accounts'
        for account in accounts.accounts
          account_data = await client_agent.getAccount account

        templates = require './templates'

        for account in [account1,account2]
          for date in ['2020-03-20','2020-04-20', '2020-05-20']

BI has issues (500 Internal Server Error) when doing too many injections, so let's not run into those.

            count = 0
            for transaction from templates date when count++ < 10
              try
                outcome = await client_agent.addTransaction account, transaction
              catch error
                console.error 'inject transaction', error.bi_message

        accounts = await client_agent.getAccounts()
        accounts.should.have.property 'balance'
        accounts.should.have.property 'accounts'
        for account in accounts.accounts
          account_data = await client_agent.getAccount account
          account_data.should.have.property 'id'
          transactions = await client_agent.getTransactions account
          transactions.should.have.property 'transactions'
          transactions.transactions.should.have.lengthOf.above 0

        await client_agent.get '/users/me/config'

        {sources} = await agent.config()
          .get "/banks/#{connection.id_connector}/sources"
        sources = sources.map ({id}) -> id

        account_types = await client_agent.get "/users/me/account_types"
        account_types.should.have.property 'accounttypes'

        accounts = await client_agent.get "/users/me/accounts"
        accounts.should.have.property 'balance'
        accounts.should.have.property 'total', 2

        return

      it 'should get webhooks_events', ->
        await agent.config().get '/webhooks_events'
        (await agent.config().get '/clients').should.have.property 'clients'
        (await agent.config().get '/monitoring').should.have.property 'gearman'
        (await agent.config().get '/webhooks/auth').should.have.property 'authproviders'

      it.skip 'should send webhooks', ->
        try console.log await agent.config().post '/test/sync'
        try console.log await agent.config().post '/test/webhooks'
        try console.log await agent.config().get '/webhooks'
        console.log await agent.users().post '/webhooks',
          event: 'USER_CREATED'
          url: 'https://p.igh.tf/user-created'

Remember to create at least one application in the console for this test to work!

      it 'should list clients', -> # aka Applications
        {clients} = await agent.config().get '/clients'
        clients[0].should.have.property 'public_key'

      it 'should enumerate balances', ->
        @timeout 200*1000
        {users} = await agent.users().getUsers()
        for {id} in users
          try
            client_agent = await agent.config().forUser id
            {balances} = await client_agent.get "/users/#{id}/balances"
            console.log id, balances.map ({balance}) -> balance
        return

PUT /users/me/connections/<id_connection>/accounts/<id_account>?all
