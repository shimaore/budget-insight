Templates pour la génération automatisée d'opérations pour injection dans BI

    Moment = require 'moment'

    module.exports = (date) ->
      date = Moment date
      start = Moment(date).startOf 'month'
      end   = Moment(date).endOf   'month'
      span = Moment.duration(start.diff end).asDays()
      console.log start.format(), end.format(), span
      for template in templates
        switch
          when template.count?
            monthly = template.count
          when template.monthly?
            monthly = Math.ceil template.monthly * (Math.random()+0.2)
          else
            monthly = 1
        for i in [1..monthly]
          amount = template.min + (template.max-template.min)*Math.random()
          date_operation = Moment(start).add Math.floor(span*Math.random()), 'days'
          date_valeur    = Moment(date_operation).add Math.floor(2*Math.random()), 'days'
          original_wording = template.original_wording
            .replace /DD\/MM/g, date_operation.format 'DD/MM'
            .replace /MMMDD/g, date_operation.format 'MMMDD'

          yield {
            original_wording
            value: Math.floor(amount*100.0)/100
            date:date_operation.format 'YYYY-MM-DD'
            rdate:date_valeur.format 'YYYY-MM-DD'
            type: template.type
          }
      return

    templates = [
      { type: 'deferred_card', original_wording: "CB DEBIT DIFFERE ALLORESTO.FR PARIS", min: -8.7, max: -19.86, monthly: 5 }
      { type: 'deferred_card', original_wording: "CB DEBIT DIFFERE DOCTEUR", min: -25, max: -25, monthly: 0 }
      { type: 'deferred_card', original_wording: "CB DEBIT DIFFERE FRANPRIX PARIS 10", min: -3.4, max: -72, monthly: 4 }
      { type: 'deferred_card', original_wording: "CB DEBIT DIFFERE GOOGLE *BUDGEA", min: -4.7, max: -8.2 }
      { type: 'deferred_card', original_wording: "CB DEBIT DIFFERE HALL'S BEER", min: -15, max: -42, monthly: 3 }
      { type: 'deferred_card', original_wording: "CB DEBIT DIFFERE MCDO", min: -7.2, max: -23, monthly: 1 }
      { type: 'deferred_card', original_wording: "CB DEBIT DIFFERE MONOPRIX", min: -13.4, max: -56, monthly: 2 }
      { type: 'deferred_card', original_wording: "CB DEBIT DIFFERE SNCF", min: -29.8, max: -123, monthly: 1 }
      { type: 'deferred_card', original_wording: "CB DEBIT DIFFERE TABAC DE LA REY PARIS", min: -4.5, max: -12, monthly: 1 }
      { type: 'deferred_card', original_wording: "CB DEBIT DIFFERE THE BOOTLAGERS PARIS", min: -8.9, max: -22.9, monthly: 1 }
      { type: 'card_summary',  original_wording: "DEBIT MENSUEL CARTE", min: -8.7, max: -8.7, monthly: 1 }
      { type: 'deposit', original_wording: "REM 4 CHQ BORNE LAILLE", min: 200, max: 850, monthly: 1 }
      { type: 'bank', original_wording: "VIR FRAIS KSR MMMDD", min: -0.80, max: -1.23, monthly: 3 }
      { type: 'transfer', original_wording: "VIR LA POMMERAIE F17001321", min: -125, max: -187, monthly: 3 }
      { type: 'transfer', original_wording: "VIR DRFIP BRETAGNE", min: 200, max: 450, monthly: 0 }

    ]

Test

    ###
    do ->
      for f from module.exports '2020-02'
        console.log f
    ###
