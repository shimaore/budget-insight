Budget Insight API
------------------

This module implements some of the functions of the [Budget Insight documentation](https://console.budget-insight.com/documentation).

    Request = require 'superagent'
    Prefix = require 'superagent-prefix'

    IDENTITY = (x) -> x
    ec = encodeURIComponent

    un = (field) ->

      get_field = (resolve,reject) -> (res) ->
        if field of res
          resolve res[field]
        else
          reject new Error "Missing field #{field} in #{res}"
        return

      self = (p) ->
        then: (resolve,reject) -> p.then (get_field resolve, reject), reject
        query: (args...) -> self p.query args...
        send: (args...) -> self p.send args...
        set: (args...) -> self p.set args...
        use: (args...) -> self p.use args...
        parse: (args...) -> self p.parse args...
        buffer: (args...) -> self p.buffer args...

      self

    unBody = un 'body'

### BudgetInsightError

    class BudgetInsightError extends Error
      constructor: ({message,response},method,path) ->
        msg = response?.body?.message ? message
        super [
          method
          path
          response?.status
          response?.text
          msg
        ].join ' → '
        @bi_message = msg
        @status = response?.status
        @code = response?.body?.code
        return

    LOG_BI_ERRORS = process.env.LOG_BI_ERRORS is 'true'

    catcher = (method,path) ->
      handler = (reject) -> (error) ->
        outcome = new BudgetInsightError error, method, path
        console.error outcome if LOG_BI_ERRORS
        reject outcome

      self = (p) ->
        then: (resolve,reject) -> p.then resolve, handler reject
        query: (args...) -> self p.query args...
        send: (args...) -> self p.send args...
        set: (args...) -> self p.set args...
        use: (args...) -> self p.use args...
        parse: (args...) -> self p.parse args...
        buffer: (args...) -> self p.buffer args...


### BudgetInsightAgent

Base class

    class BudgetInsightAgent
      constructor: (prefix,options) ->
        throw new Error 'Invalid prefix' unless 'string' is typeof prefix and prefix.match /^http/
        throw new Error 'Invalid client_id' unless 'string' is typeof options.client_id
        throw new Error 'Invalid client_secret' unless 'string' is typeof options.client_secret

        @agent = Request.agent()
          .use Prefix prefix
          .type 'json'
          .accept 'json'

        @__prefix = prefix
        @__options = options
        return

      client_id:      -> @__options.client_id
      client_secret:  -> @__options.client_secret

Generic methods

      get: (path) ->
        query = @agent.get path
        (catcher 'GET', path) unBody query

      post: (path) ->
        query = @agent.post path
        (catcher 'POST', path) unBody query

      put: (path) ->
        query = @agent.put path
        (catcher 'PUT', path) unBody query

Budget-Insight-specific methods

      getBanks: -> @getConnectors()
      getBank: (id) -> @getConnector id

      getConnectors: ->
        (un 'connectors') @get '/connectors'
      getConnector: (id) ->
        @get "/connectors/#{ec id}"


      getUsers: ->
        @get "/users"

      getUser: (id) ->
        @get "/users/#{ec id}"

      addPermanentUser: ->
        {id_user,auth_token} = await @post '/auth/init'
          .send
            client_id: @client_id()
            client_secret: @client_secret()
        @impersonate id_user, auth_token

      impersonate: (id_user,auth_token) ->
        client = @clientAgentClass()
        client_agent = new client @__prefix, @__options, id_user, auth_token

Feel free to override the `clientAgentClass` with your own
(typically a subclass of UserBudgetInsightAgent).

      clientAgentClass: -> UserBudgetInsightAgent

WebHooks:
- `/config` with `autosync.frequency`

      addWebhook: (id_user,id_service,id_auth,url,event,params) ->
        # params= {type,key,value} ??
        @post '/webhooks'
        .send {id_user,id_service,id_auth,url,event,params}

      addWebhookAuth: (type,name,config={}) ->
        @post '/webhooks/auth'
        .send {type,name,config}

Also: /webhooks/:id_webhook/add_to_data/:key …

### AuthBudgetInsightAgent

An authenticated agent

    class AuthBudgetInsightAgent extends BudgetInsightAgent
      constructor: (prefix,options,auth_token) ->
        super prefix, options
        @agent = @agent
          .set Authorization: "Bearer #{auth_token}"
        @auth_token = auth_token
        return

      forUser: (id) ->
        {jwt_token,payload} = await @post '/auth/jwt'
          .send
            client_id: @client_id()
            client_secret: @client_secret()
            scope: 'user'
            id_user: id
            expire: false
        # console.log "Received JWT for #{id}", payload
        client = @clientAgentClass()
        client_agent = new client @__prefix, @__options, id, jwt_token

### UserBudgetInsightAgent

An agent authenticated as an end-user

    class UserBudgetInsightAgent extends AuthBudgetInsightAgent
      constructor: (prefix,options,id_user,auth_token) ->
        super prefix, options, auth_token
        @id_user = id_user
        return

      addConnection: (login,password,id_bank,params = {}) ->
        params = Object.assign {login,password,id_bank}, params
        {id} = await @post "/users/#{ec @id_user}/connections"
          .send params
        await @put "/users/#{ec @id_user}/connections/#{ec id}"

Synchronously fetch all data

      getConnections: ->
        @get "/users/#{ec @id_user}/connections"

      getAccounts: ->
        @get "/users/#{ec @id_user}/accounts"

      getAccount: (account) ->
        @get "/users/#{ec @id_user}/accounts/#{ec account.id}"

      addAccount: (connection,name,number,balance,type='unknown') ->
        @post "/users/#{ec @id_user}/connections/#{ec connection.id}/accounts"
        .send {name,number,balance,type}

      getTransactions: (account) ->
        @get "/users/#{ec @id_user}/accounts/#{ec account.id}/transactions"

      addTransaction: (account,transaction) ->
        @post "/users/#{ec @id_user}/accounts/#{ec account.id}/transactions"
        .send transactions: [transaction]

    module.exports = {
      BudgetInsightError
      BudgetInsightAgent
      AuthBudgetInsightAgent
      UserBudgetInsightAgent
    }
