Budget Insight API
------------------

This module implements some of the functions of the [Budget Insight documentation](https://console.budget-insight.com/documentation).

    {BudgetInsightAgent,AuthBudgetInsightAgent} = require './api'

    module.exports = (url,{client_id,client_secret,config_token,users_token,invoicing_token}) ->
      options = {client_id,client_secret}
      agent = -> new BudgetInsightAgent url, options
      agent.config    = -> new AuthBudgetInsightAgent url, options, config_token    if config_token?
      agent.users     = -> new AuthBudgetInsightAgent url, options, users_token     if users_token?
      agent.invoicing = -> new AuthBudgetInsightAgent url, options, invoicing_token if invoicing_token?
      agent
